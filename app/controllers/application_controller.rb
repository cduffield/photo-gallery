class ApplicationController < ActionController::Base
	before_filter :getCategoryNav
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def getCategoryNav
  	@categoryNav = Category.all
  end 

  def logged_in?
      current_user
    end
    helper_method :logged_in?
    def current_user
    	@current_user ||= User.find(session[:user_id]) if session[:user_id] 
    end 

    def require_user
    	if current_user 
    		true
    	else
    		redirect_to login_path, notice: "You must be logged in to access that page." 
    	end 
    end 

    def require_admin
      if current_user.admin == true 
        true
      else 
        false
      end 
    end 
end
