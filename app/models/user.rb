class User < ActiveRecord::Base
	has_secure_password

	validates :email, presence: true,
					  uniqueness: true,
					  format: {
					  	with: /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9\.-]+\.[A-Za-z]+\Z/
					  }
	validates :first_name, presence: true
	validates :last_name, presence: true

	before_save :downcase_email 


	def full_name 
		self.first_name + " " + self.last_name
	end 

	def downcase_email 
		self.email = email.downcase
	end 
end
