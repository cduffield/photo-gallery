# Description 

Photo Gallery.  
Upload photos using Carrierwave. 
Store on Amazon S3 using fog gem. 
Display photos using lightbox. 
Bcrypt authentication. 